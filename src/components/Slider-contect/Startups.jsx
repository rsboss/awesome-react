//Slider-contecr Folder in Startups.jsx

import React from 'react';





function Startups(props){
    return(
        <div className="container-fluid">
            <div className="hero-inner section-inner">
                <div className="hero-content">
                    <h1>{props.title} <span>{props.title2}</span></h1>
                    <p className="reveal-from">{props.para }<br />{props.parar}</p>
                    <div className="button-group">
                    <a href="" className="button2">{props.button2}</a>
                    <a href="" className="button3">{props.button3}</a>
                    <div className="viedo-play">
                    <img className="viedo" src={props.viedo} />
                </div>
                </div>
            </div>
            </div>
        </div>
    );
};

export default Startups;
