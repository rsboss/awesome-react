//Content-Card-data Folder in Cardlist.jsx
const Sdata = [

    {
        id:1,
        imgessrc:"https://open.cruip.com/static/media/feature-tile-icon-01.0f9928d7.svg",
        titles:"Robust Workflow",
        titles1:"Duis aute irure dolor in reprehenderit",
        titles2:"in voluptate velit esse cillum dolore",
        titles3:"eu fugiat nulla pariatur. Excepteur sint",
        titles4:"occaecat cupidatat.",
    
    },
    {
        id:2,
        imgessrc:"https://open.cruip.com/static/media/feature-tile-icon-02.bd39f304.svg",
        titles:"Robust Workflow",
        titles1:"Duis aute irure dolor in reprehenderit",
        titles2:"in voluptate velit esse cillum dolore",
        titles3:"eu fugiat nulla pariatur. Excepteur sint",
        titles4:"occaecat cupidatat.",
     
    },
    {
        id:3,
        imgessrc:"https://open.cruip.com/static/media/feature-tile-icon-03.66f37ba5.svg",
        titles:"Robust Workflow",
        titles1:"Duis aute irure dolor in reprehenderit",
        titles2:"in voluptate velit esse cillum dolore",
        titles3:"eu fugiat nulla pariatur. Excepteur sint",
        titles4:"occaecat cupidatat.",
     
    },
    {
        id:4,
        imgessrc:"https://open.cruip.com/static/media/feature-tile-icon-04.836acd10.svg",
        titles:"Robust Workflow",
        titles1:"Duis aute irure dolor in reprehenderit",
        titles2:"in voluptate velit esse cillum dolore",
        titles3:"eu fugiat nulla pariatur. Excepteur sint",
        titles4:"occaecat cupidatat.",
     
    },
    {
        id:5,
        imgessrc:"https://open.cruip.com/static/media/feature-tile-icon-05.fa9ba00b.svg",
        titles:"Robust Workflow",
        titles1:"Duis aute irure dolor in reprehenderit",
        titles2:"in voluptate velit esse cillum dolore",
        titles3:"eu fugiat nulla pariatur. Excepteur sint",
        titles4:"occaecat cupidatat.",
     
    },
    {
        id:6,
        imgessrc:"https://open.cruip.com/static/media/feature-tile-icon-06.6a177696.svg",
        titles:"Robust Workflow",
        titles1:"Duis aute irure dolor in reprehenderit",
        titles2:"in voluptate velit esse cillum dolore",
        titles3:"eu fugiat nulla pariatur. Excepteur sint",
        titles4:"occaecat cupidatat.",
     
    },
    
       
        ];
    
        export default Sdata;