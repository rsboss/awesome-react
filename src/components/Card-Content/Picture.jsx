//Card-contecr Folder in Picture.jsx
import React from 'react';


function Picture(props){
    return(
        <div className="container-fluid">
            <div className="container">
                <div className="picture-sec">
                    <div className="picture">
                        <h2>{props.picture}</h2>
                            <p>{props.para1}<br />{props.para2}<br />{props.para3}</p>
                    </div>
                </div>
            </div>
        </div>
     );
  };

export default Picture;