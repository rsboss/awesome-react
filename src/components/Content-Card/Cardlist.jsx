//Content-Card Folder in Cardlist.jsx
import React from 'react';


function Cardlist(props) {
    
    //console.log(props);
    return(      
    <div className="cardlist"> 
        <div className="container">
            <div className="img-sec">
            <img src={props.imgessrc} alt="myPic" className="card_img" />
            <div className="card_info">
                <h3 className="card_category">{props.titles}</h3>
                <p className="card_title">{props.titles1}<br />{props.titles2}<br />{props.titles3}<br />{props.titles4}</p>
            </div>
            
        </div>
        </div>
    </div> 
    );
}

export default Cardlist;



